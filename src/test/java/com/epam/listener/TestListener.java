package com.epam.listener;

import com.epam.GmailSendEmailTest;
import com.epam.utils.DriverManager;
import com.google.common.io.Files;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


import java.io.File;
import java.io.IOException;

public class TestListener implements ITestListener {

    private static final Logger LOG = LogManager.getLogger(GmailSendEmailTest.class);

    private static String getTestMethodName(ITestResult result){
        return result.getMethod().getConstructorOrMethod().getName();
    }

    @Attachment(type = "image/png")
    public static byte[] makeScreenshot(WebDriver driver)/* throws IOException */ {
        try {
            File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            return Files.toByteArray(screen);
        } catch (IOException e) {
            return null;
        }
    }

    @Attachment(value = "{0}", type = "text/plain")
    private String makeTextLog(String log) {
        return log;
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void  onTestFailure(ITestResult result){
        LOG.info("Test Failure!!! Screenshot captured for test case: "
                + getTestMethodName(result));
        makeScreenshot(DriverManager.getDriver());
        makeTextLog(getTestMethodName(result) + " and screenshot taken!");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
