package com.epam;

import com.epam.bo.AuthorizationPageBO;
import com.epam.bo.HomePageBO;
import com.epam.bo.WelcomePageBO;
import com.epam.listener.TestListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.epam.utils.DriverManager.getDriver;
import static com.epam.utils.DriverManager.quitDriver;
import static com.epam.utils.Property.getProperty;
import static org.testng.AssertJUnit.assertEquals;

@Listeners({TestListener.class})
public class GmailSendEmailTest {

    private WelcomePageBO welcomePage = new WelcomePageBO();

    private HomePageBO homePage = new HomePageBO();

    private AuthorizationPageBO authorizationPageBO = new AuthorizationPageBO();

    @BeforeMethod
    public void setUp() {
        getDriver();
    }

    @Test
    public void testWelcome() {
        authorizationPageBO.authorizationOfNewGmailUser(getProperty("SOME_EMAIL_LOGIN_3"), getProperty("SOME_EMAIL_PASSWORD_3"));
        welcomePage.skipWelcomePage();
        homePage.createAndSendEmail(getProperty("SOME_EMAIL_LOGIN_1"), getProperty("EMAIL_SUBJECT"), getProperty("EMAIL_MESSAGE"));
        homePage.checkWhetherMessageIsInSentDirectory();
        assertEquals(homePage.getActualUSTime() + " View details", homePage.getMessageValue());
        homePage.deleteLastMessage();
        assertEquals("1 deleted", homePage.getProofOfDeletingMessage());
    }

    @AfterMethod
    public void endTest() {
        quitDriver();
    }
}
