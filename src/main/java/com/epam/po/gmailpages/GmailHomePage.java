package com.epam.po.gmailpages;

import com.epam.bo.WelcomePageBO;
import com.epam.po.GeneralPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.Property.getProperty;

public class GmailHomePage extends GeneralPage {

    private static final Logger LOG = LogManager.getLogger(WelcomePageBO.class);

    @FindBy(id="com.google.android.gm:id/compose_button")
    private WebElement composeButton;

    @FindBy(id="com.google.android.gm:id/to")
    private WebElement fieldTo;

    @FindBy(id="com.google.android.gm:id/subject")
    private WebElement fieldSubject;

    @FindBy(id="com.google.android.gm:id/body")
    private WebElement emailTextField;

    @FindBy(id="com.google.android.gm:id/send")
    private WebElement sendEmailButton;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private WebElement menuButton;

    @FindBy(xpath = "//android.widget.ListView/android.widget.LinearLayout[8]")
    private WebElement sentDirectoryButton;

    @FindBy(xpath = "//android.widget.ListView/android.widget.FrameLayout[1]/android.view.View")
    private WebElement lastSentMessage;

    @FindBy(id = "com.google.android.gm:id/send_date")
    private WebElement messageSentTimeTextArea;

    @FindBy(id = "com.google.android.gm:id/delete")
    private WebElement deleteMessageButton;

    @FindBy(id = "com.google.android.gm:id/description_text")
    private WebElement popUpDeleteTextLabel;

    @FindBy(id = "com.google.android.gm:id/conversation_list_place_holder")
    private WebElement messagesList;

    public void clickComposeButton() {
        LOG.info("Click 'Compose' button");
        (new WebDriverWait(driver, Integer.valueOf(getProperty("DEFAULT_IMPLICITLY_WAIT_TIME")))).until(ExpectedConditions.visibilityOf(messagesList));
        composeButton.click();
    }

    public void fillToField(String to) {
        LOG.info("Filling receiver email");
        fieldTo.sendKeys(to);
    }

    public void fillSubjectField(String subject) {
        LOG.info("Filling email`s subject field");
        fieldSubject.sendKeys(subject);
    }

    public void fillEmailText(String emailText) {
        LOG.info("Typing email`s text");
        emailTextField.sendKeys(emailText);
    }

    public void clickSendButton() {
        LOG.info("Sending email");
        sendEmailButton.click();
    }

    public String getReceiverValue() {
        return fieldTo.getText();
    }

    public void clickMenuButton(){
        LOG.info("Click 'Menu' button");
        menuButton.click();
    }

    public void clickSentDirectoryButton(){
        (new WebDriverWait(driver, Integer.valueOf(getProperty("DEFAULT_IMPLICITLY_WAIT_TIME")))).until(ExpectedConditions.visibilityOf(sentDirectoryButton));
        LOG.info("Click 'Sent' directory button");
        sentDirectoryButton.click();
    }

    public void clickLastSentMessage(){
        LOG.info("Click on last sent message");
        lastSentMessage.click();
    }

    public String getLastSentMessageTime(){
        return messageSentTimeTextArea.getText();
    }

    public void deleteLastMessage(){
        LOG.info("Click 'Delete' button");
        deleteMessageButton.click();
    }

    public String getProofOfDeletingMessage(){
        return popUpDeleteTextLabel.getText();
    }
}
