package com.epam.po.gmailpages;

import com.epam.po.GeneralPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.Property.getProperty;

public class GmailWelcomePage extends GeneralPage {

    private static final Logger LOG = LogManager.getLogger(GmailWelcomePage.class);

    @FindBy(id="com.google.android.gm:id/setup_addresses_list")
    private WebElement adressesList;

    @FindBy(id="com.google.android.gm:id/action_done")
    private WebElement doneButton;

    public void clickDoneButton() {
        (new WebDriverWait(driver, Integer.valueOf(getProperty("DEFAULT_IMPLICITLY_WAIT_TIME")))).until(ExpectedConditions.visibilityOf(adressesList));
        LOG.info("Clicking 'DONE' button");
        doneButton.click();
    }

}
