package com.epam.po.gmailpages;

import com.epam.bo.WelcomePageBO;
import com.epam.po.GeneralPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailAuthorizationPage extends GeneralPage{

    private static final Logger LOG = LogManager.getLogger(WelcomePageBO.class);

    @FindBy(id = "welcome_tour_got_it")
    private WebElement gotItButton;

    @FindBy(id = "com.google.android.gm:id/setup_addresses_add_another")
    private WebElement addAnotherEmailButton;

    @FindBy(id = "com.google.android.gm:id/google_option")
    private WebElement addEmailViaGoogleOption;

    @FindBy(id = "com.google.android.gm:id/suw_navbar_next")
    private WebElement nextButton;

    @FindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/" +
            "android.view.View/android.view.View[1]/android.view.View[1]/android.widget.EditText")
    private WebElement emailTextArea;

    @FindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private WebElement nextEmailButton;

    @FindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.EditText")
    private WebElement passwordTextArea;

    @FindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private WebElement nextPasswordButton;

    @FindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private WebElement agreeButton;

   public void clickGotItButton(){
       LOG.info("Clicking 'Got It' button");
       gotItButton.click();
   }

   public void clickAddAnotherEmailButton(){
       LOG.info("Clicking 'Add another email address' button");
       addAnotherEmailButton.click();
   }

   public void chooseAddEmailViaGoogleOption(){
       LOG.info("Clicking 'Google' option");
       addEmailViaGoogleOption.click();
   }

   public void clickNextButton(){
       LOG.info("Clicking 'Next' button");
       nextButton.click();
   }

   public void fillEmailTextArea(String email){
       LOG.info("Filling 'Email' TextArea");
       emailTextArea.sendKeys(email);
   }

   public void clickNextEmailButton(){
       LOG.info("Clicking 'Next(Email)' button");
       nextEmailButton.click();
   }

   public void fillPasswordTextArea(String password){
       LOG.info("Filling 'Password' TextArea");
       passwordTextArea.sendKeys(password);
   }

   public void clickNextPasswordButton(){
       LOG.info("Clicking 'Next(Password)' button");
       nextPasswordButton.click();
   }

   public void clickAgreeButton(){
       LOG.info("Clicking 'Agree' button");
       agreeButton.click();
   }
    }
