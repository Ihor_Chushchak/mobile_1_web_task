package com.epam.po;

import com.epam.utils.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.Property.getProperty;

public abstract class GeneralPage {

    protected WebDriver driver;
    protected WebDriverWait webDriverWait;

    public GeneralPage() {
        driver = DriverManager.getDriver();
        webDriverWait = new WebDriverWait(driver, Integer.valueOf(getProperty("DEFAULT_IMPLICITLY_WAIT_TIME")));
        PageFactory.initElements(driver, this);
    }
}

