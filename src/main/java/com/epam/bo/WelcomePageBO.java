package com.epam.bo;

import com.epam.po.gmailpages.GmailWelcomePage;

public class WelcomePageBO {

    private GmailWelcomePage welcomePage = new GmailWelcomePage();

    public void skipWelcomePage() {
        welcomePage.clickDoneButton();
    }
}
