package com.epam.bo;

import com.epam.po.gmailpages.GmailHomePage;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class HomePageBO {

    private GmailHomePage homePage = new GmailHomePage();

    public String fillReceiverInfo(String receiver) {
        return homePage.getReceiverValue();
    }

    public void createAndSendEmail(String receiver, String subject, String emailText) {
        homePage.clickComposeButton();
        homePage.fillToField(receiver);
        homePage.fillSubjectField(subject);
        homePage.fillEmailText(emailText);
        homePage.clickSendButton();
    }

    public void checkWhetherMessageIsInSentDirectory() {
        homePage.clickMenuButton();
        homePage.clickSentDirectoryButton();
        homePage.clickLastSentMessage();
    }

    public void deleteLastMessage() {
        homePage.deleteLastMessage();
    }

    public String getMessageValue() {
        return homePage.getLastSentMessageTime();
    }

    public String getProofOfDeletingMessage() {
        return homePage.getProofOfDeletingMessage();
    }

    public String getActualUSTime() {
        LocalTime localTime = LocalTime.now(ZoneId.of("Europe/London"));
        Locale locale_en_US = Locale.US;
        return localTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(locale_en_US));
    }

}
