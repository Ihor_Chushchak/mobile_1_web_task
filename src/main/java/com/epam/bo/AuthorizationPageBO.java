package com.epam.bo;

import com.epam.po.gmailpages.GmailAuthorizationPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AuthorizationPageBO {

        private static final Logger LOG = LogManager.getLogger(WelcomePageBO.class);

        private GmailAuthorizationPage authorizationPage = new GmailAuthorizationPage();

        public void authorizationOfNewGmailUser(String email,String password){
            authorizationPage.clickGotItButton();
            authorizationPage.clickAddAnotherEmailButton();
            authorizationPage.chooseAddEmailViaGoogleOption();
            authorizationPage.clickNextButton();
            authorizationPage.fillEmailTextArea(email);
            authorizationPage.clickNextEmailButton();
            authorizationPage.fillPasswordTextArea(password);
            authorizationPage.clickNextPasswordButton();
            authorizationPage.clickAgreeButton();

        }
}
