package com.epam.utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.epam.utils.Property.getProperty;

public class DriverManager {

    private static final Logger LOG = LogManager.getLogger(DriverManager.class);

    private static AndroidDriver<WebElement> driver;
    private static URL url;

    private DriverManager() {
    }

    static {
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static AndroidDriver<WebElement> getDriver() {
        if (Objects.isNull(driver)) {
            LOG.info("Creating driver instance");
            setUpSettings();
        }
        return driver;
    }

    private static void setUpSettings() {
        LOG.info("Setting up driver");
        driver = new AndroidDriver(url, setCapabilities());
        driver.manage().timeouts().implicitlyWait(Integer.valueOf(getProperty("NON_DEFAULT_IMPLICITLY_WAIT_TIME")), TimeUnit.SECONDS);
    }

    private static DesiredCapabilities setCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, getProperty("PLATFORM"));
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, getProperty("DEVICE"));
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, getProperty("APP_PACK"));
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, getProperty("APP_ACTIVITY"));
        return capabilities;
    }

    public static void quitDriver() {
        LOG.info("Quiting driver");
        getDriver().quit();
    }

}
